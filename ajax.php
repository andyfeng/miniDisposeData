<?php
$param = $_REQUEST['input_value'];
$result = '';
if(!empty($param)){
	
	$spaceFlag = "\t";
	$arr = explode("、",$param);
	$type_name = array();
	
	foreach($arr as $key => $info)
	{
		list($name,$type) = explode('-',$info);
		$type_name[$type][] = $info;
	}
	//title
	$allType = array_keys($type_name);
	//$title = implode($spaceFlag, $allType);

	$typeCol = array();
	$result = array();
	foreach($allType as $key => $v){
		$result[0][$key] = $v;
		$typeCol[$v] = $key;
	}
	foreach ($type_name as $key => $nameArr) {
		$step = 1;
		$col = $typeCol[$key];
		foreach($nameArr as $kk => $vv){
			$result[$step ++][$col] = $vv;
		}
	}

	$finalResult = '';
	$colCount = count($allType);
	foreach($result as $k => $v){
		if($k == 0){
			$finalResult = implode($spaceFlag, $v)."\r\n";
			continue;
		}
		for($i = 0; $i < $colCount; $i ++){
			$finalResult .= empty($v[$i]) ? '' : $v[$i];
			if($i == ($colCount - 1) ){
				$finalResult .= "\r\n";
			}else{
				$finalResult .= $spaceFlag;
			}
		}
		//$finalResult .= "\r\n";
	}
	if($_REQUEST['from']){
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=yzp_data_".date('Y-m-d/H:i:s').".xls");
		header ( "Pragma: no-cache" );
		header ( "Expires: 0" );
		$finalResult = mb_convert_encoding ( $finalResult, "GBK", "UTF-8" );
	}
	echo $finalResult;
}
?>